<?php
$str = '1234567';
$price_length = strlen($str)-2;
$price_part1 = substr($str, 0, -2);
$price_part2 = substr($str, -2);
$vars['items'][0]['#markup'] = 'sdsd';

echo $price_part1.'----'.$price_part2;


<?php
/**
 * @file
 * Modifies price field output for Book content type.
 */

/**`
 * Implements hook_preprocess_field().
 */
function book_price_preprocess_field(&$vars) {
  if (isset($vars['element']['#field_name']) && $vars['element']['#field_name']
    == 'field_price' && !empty($vars['items'][0]['#markup'])
  ) {
    $price_part1 = substr($vars['items'][0]['#markup'], 0, -2);
    $price_part2 = substr($vars['items'][0]['#markup'],-2);
    $vars['items'][0] = array(
      '#markup'=>$price_part1.'<sup>'.$price_part2.'</sup>'
    );
  }
}

